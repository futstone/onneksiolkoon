particlesJS('particles-js', {
  particles: {
    number: { value: 200, density: { enable: false, value_area: 800 } },
    color: { value: '#ffffff' },
    shape: {
      type: 'star',
      stroke: { width: 0, color: '#000000' },
      polygon: { nb_sides: 5 },
      image: {
        src:
          'http://wiki.lexisnexis.com/academic/images/f/fb/Itunes_podcast_icon_300.jpg',
        width: 100,
        height: 100
      }
    },
    opacity: {
      value: 0.5,
      random: true,
      anim: { enable: false, speed: 1, opacity_min: 0.1, sync: false }
    },
    size: {
      value: 4,
      random: true,
      anim: { enable: false, speed: 40, size_min: 0.1, sync: false }
    },
    line_linked: {
      enable: false,
      distance: 150,
      color: '#ffffff',
      opacity: 0.4,
      width: 1
    },
    move: {
      enable: true,
      speed: 8,
      direction: 'left',
      random: true,
      straight: true,
      out_mode: 'out',
      bounce: false,
      attract: { enable: false, rotateX: 600, rotateY: 1200 }
    }
  },
  interactivity: {
    detect_on: 'canvas',
    events: {
      onhover: { enable: true, mode: 'grab' },
      onclick: { enable: true, mode: 'repulse' },
      resize: true
    },
    modes: {
      grab: { distance: 200, line_linked: { opacity: 1 } },
      bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
      repulse: { distance: 200, duration: 0.4 },
      push: { particles_nb: 4 },
      remove: { particles_nb: 2 }
    }
  },
  retina_detect: true
})

let bounce = new Bounce()
let bounce2 = new Bounce()

bounce
  .scale({
    from: { x: 1, y: 1 },
    to: { x: 0.2, y: 0.2 },
    easing: 'bounce',
    duration: 1000,
    stiffness: 1,
    delay: 0
  })
  .scale({
    from: { x: 1, y: 1 },
    to: { x: 5, y: 5 },
    bounces: 6,
    easing: 'bounce',
    duration: 1000,
    stiffness: 1,
    delay: 200
  })
  .applyTo(document.querySelectorAll('.top'), { loop: true })

bounce2
  .scale({
    from: { x: 1, y: 1 },
    to: { x: 0.75, y: 2 },
    bounces: 1,
    easing: 'bounce',
    duration: 400,
    stiffness: 1,
    delay: 0
  })
  .scale({
    from: { x: 1, y: 1 },
    to: { x: 1.5, y: 0.5 },
    bounces: 4,
    easing: 'bounce',
    duration: 2200,
    stiffness: 1,
    delay: 300
  })
  .applyTo(document.querySelectorAll('.bottom'), { loop: true })
